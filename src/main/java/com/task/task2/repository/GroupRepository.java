package com.task.task2.repository;

import com.task.task2.models.Group;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
@Repository
public interface GroupRepository extends CrudRepository<Group, Integer> {
}
