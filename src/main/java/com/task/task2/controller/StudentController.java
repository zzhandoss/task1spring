package com.task.task2.controller;

import com.task.task2.repository.StudentRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class StudentController {
    private StudentRepository studentRepository;

    @GetMapping("api/students")
    public ResponseEntity<?> getAllStudents(){
        return ResponseEntity.ok(studentRepository.findAll());
    }
    @GetMapping("api/students/groups")
    public ResponseEntity<?> getAllStudentsByGroups(){
        return ResponseEntity.ok(studentRepository.findAll());
    }
}
