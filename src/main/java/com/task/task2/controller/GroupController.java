package com.task.task2.controller;

import com.task.task2.models.Group;
import com.task.task2.repository.GroupRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class GroupController {
    private GroupRepository groupRepository;

    @GetMapping("api/groups")
    public ResponseEntity<?> GetAllGroups(){
        return ResponseEntity.ok(groupRepository.findAll());
    }
}

